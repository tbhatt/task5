FROM openjdk:14
ADD target/Task5-0.0.1-SNAPSHOT.jar task5.jar
ENTRYPOINT [ "java", "-jar", "/task5.jar"]
