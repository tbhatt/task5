# Music database with thymeleaf and endpoint API
#### LINK: https://noroff-task5api.herokuapp.com/
There are 2 main features in this program. 
1. Thymeleaf
    1. A homepage with 5 random Artists, Songs and Genres.
    2. Search field to find details for a song.
2. API Get and Posts
    1. Get all customers: /api/customers
    2. Add a new customer: /api/customers/add
    3. Update an existing customer: /api/customers/update
    4. Return the number of customers in each country: /api/customers/country
    5. Customers who are the highest spenders: /api/customers/billing
    6. Most popular genres for customers: /api/customers/genre