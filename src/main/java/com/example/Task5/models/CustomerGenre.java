package com.example.Task5.models;

public class CustomerGenre {
    public int customerId;
    public String genre;
    public int count;

    public CustomerGenre(int customerId, String genre, int count) {
        this.customerId = customerId;
        this.genre = genre;
        this.count = count;
    }
}
