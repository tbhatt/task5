package com.example.Task5.models;

public class SongInfo {
    public String name;
    public String artist;
    public String album;
    public String genre;

    public SongInfo(String n, String ar, String a, String g){
        name = n;
        artist = ar;
        album = a;
        genre = g;
    }
}
