package com.example.Task5.models;

public class CountryStats {
    public String country;
    public int count;

    public CountryStats(String country, int count) {
        this.country = country;
        this.count = count;
    }
}
