package com.example.Task5.models;

public class Customer {
    public int id;
    public String firstName;
    public String lastName;
    public String country;
    public String postalCode;
    public String  phone;

    public Customer(int id, String firstName, String lastName, String country, String postalCode, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
    }

}
