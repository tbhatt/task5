package com.example.Task5.models;

public class CustomerFull {
    public int customerId;
    public String firstName;
    public String lastName;
    public String company;
    public String address;
    public String city;
    public String state;
    public String country;
    public String postalCode;
    public String phone;
    public String fax;
    public String email;
    public int supportRepId = 1;

    public CustomerFull(String firstName, String lastName, String company, String address, String city, String state, String country, String postalCode, String phone, String fax, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.fax = fax;
        this.email = email;
    }


}
