package com.example.Task5.models;

public class BillingStats {
    public int id;
    public int sum;

    public BillingStats(int id, int sum) {
        this.id = id;
        this.sum = sum;
    }
}
