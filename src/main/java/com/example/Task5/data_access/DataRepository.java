package com.example.Task5.data_access;

import com.example.Task5.models.SongInfo;

import java.sql.*;
import java.util.ArrayList;

public class DataRepository {
    // Setting up the connection object we need.

    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public SongInfo getSong(String s){
        System.out.println("Getting song: " + s);
        //String song = "";
        SongInfo song = null;
        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep =
                    conn.prepareStatement("SELECT t.name as name, a.Title as album, g.Name as genre, ar.Name as artist " +
                                              "FROM Track as t " +
                                              "INNER JOIN Album as a on t.AlbumId == a.AlbumId " +
                                              "INNER JOIN Genre as g on t.GenreId == g.GenreId " +
                                              "INNER JOIN Artist as ar on a.ArtistId == ar.ArtistId " +
                                              "WHERE t.name = '"+s+"' " +
                                              "LIMIT 1");
            ResultSet set = prep.executeQuery();

            song = new SongInfo(
                    set.getString("name"),
                    set.getString("artist"),
                    set.getString("album"),
                    set.getString("genre")
            );

            System.out.println("set:  " + set);

            //Test set
            /*
            ResultSetMetaData rsmd = set.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while(set.next()){
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = set.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                }
                System.out.println("");
            }*/

        } catch (Exception e){
            System.out.println(e);
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return song;

    }
    public ArrayList<String> getRandomArtists(){
        ArrayList<String> artists = new ArrayList<String>();
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Name FROM Artist ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                artists.add(set.getString("Name"));
            }

        } catch (Exception e){
            System.out.println(e);
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        System.out.println("Got artists" + artists);
        return artists;
    }

    public ArrayList<String> getRandomSongs(){
        ArrayList<String> songs = new ArrayList<String>();
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Name FROM Track ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                songs.add(set.getString("Name"));
            }

        } catch (Exception e){
            System.out.println(e);
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return songs;
    }

    public ArrayList<String> getRandomGenres(){
        ArrayList<String> genres = new ArrayList<String>();
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Name FROM Genre ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                genres.add(set.getString("Name"));
            }

        } catch (Exception e){
            System.out.println(e);
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return genres;
    }

}
