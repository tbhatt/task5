package com.example.Task5.data_access;

import com.example.Task5.models.*;

import java.sql.*;
import java.util.ArrayList;

public class CustomerAccess {
    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        Customer customer;
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone " +
                                              "FROM Customer");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customer = new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone")
                        );
                customers.add(customer);
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    public ArrayList<CountryStats> getCountryCount(){
        ArrayList<CountryStats> countries= new ArrayList<>();
        CountryStats country;
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Country, count(*) as Count FROM Customer GROUP BY Country ORDER BY Count DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                country = new CountryStats(
                        set.getString("Country"),
                        set.getInt("Count")
                );
                countries.add(country);
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return countries;
    }

    public ArrayList<BillingStats> getTotalBilling(){
        ArrayList<BillingStats> billings= new ArrayList<>();
        BillingStats billing;
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT CustomerId, sum(Total) as Sum FROM Invoice GROUP BY CustomerId ORDER BY Sum DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                billing = new BillingStats(
                        set.getInt("CustomerId"),
                        set.getInt("Sum")
                );
                billings.add(billing);
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return billings;

    }

    public ArrayList<CustomerGenre> getCustomerGenre(){
        ArrayList<CustomerGenre> favGenres= new ArrayList<>();
        CustomerGenre favGenre;
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT c.CustomerId, g.GenreId, g.Name, count(*) as Count " +
                                              "FROM Customer AS c " +
                                              "INNER JOIN Invoice AS i ON i.CustomerId = c.CustomerId " +
                                              "INNER JOIN InvoiceLine as il ON i.InvoiceId = il.InvoiceId " +
                                              "INNER JOIN Track as t on il.TrackId = t.trackId " +
                                              "INNER JOIN Genre as g on t.GenreId = g.GenreId " +
                                              "GROUP BY c.CustomerId, g.GenreId " +
                                              "ORDER BY c.CustomerId, count(*) DESC " );
            ResultSet set = prep.executeQuery();

            //Only adds the highest counting genres for each customer.
            int customerId = -1;
            int highestCount = 0;
            while(set.next()){
                //First if tests if we have come to a new customer. Since we have sorted this is the highest count
                if(set.getInt("CustomerId") != customerId){
                    customerId = set.getInt("CustomerId");
                    highestCount = set.getInt("Count");
                    favGenre = new CustomerGenre(
                            set.getInt("CustomerId"),
                            set.getString("Name"),
                            set.getInt("Count")
                    );
                    favGenres.add(favGenre);
                }
                //Test if a customer has other genres with same count as the highest
                else if(set.getInt("Count") == highestCount ){
                    favGenre = new CustomerGenre(
                            set.getInt("CustomerId"),
                            set.getString("Name"),
                            set.getInt("Count")
                    );
                    favGenres.add(favGenre);
                }
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return favGenres;
    }

    public Customer updateCustomer(CustomerFull customer){
        Customer result = null;
        try{
            conn = DriverManager.getConnection(URL);
            String statement = "UPDATE Customer";
            statement += buildUpdateQuery(customer);
            statement += " WHERE CustomerId = " + customer.customerId;

            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(statement);
            prep.executeUpdate();

            prep = conn.prepareStatement("SELECT * FROM Customer WHERE CustomerId = " + customer.customerId);
            ResultSet set = prep.executeQuery();
            set.next();
            result = new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone")
                    );

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return result;

    }

    public int addCustomer(CustomerFull customer){
        try{
            customer.customerId = getVacantId();

            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement("INSERT INTO Customer" +
                                             "(CustomerId, FirstName, LastName, Company, Address, City, " +
                                             "State, Country, PostalCode, Phone, Fax, Email, SupportRepId) " +
                                             "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            prep.setInt(1, customer.customerId);
            prep.setString(2, customer.firstName);
            prep.setString(3, customer.lastName);
            prep.setString(4, customer.company);
            prep.setString(5, customer.address);
            prep.setString(6, customer.city);
            prep.setString(7, customer.state);
            prep.setString(8, customer.country);
            prep.setString(9, customer.postalCode);
            prep.setString(10, customer.phone);
            prep.setString(11, customer.fax);
            prep.setString(12, customer.email);
            prep.setInt(13, customer.supportRepId);

            int res = prep.executeUpdate();
            System.out.println("Result of adding customer: " + customer.customerId + " - " + res);

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }

        return customer.customerId;
    }

    //Helper methods
    public int getVacantId(){
        int freeId = -1;
        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT MAX(CustomerId) AS id FROM Customer LIMIT 1");
            ResultSet set = prep.executeQuery();
            set.next();
            freeId = set.getInt("id") + 1;

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        System.out.println("Free ID is " + freeId);
        return freeId;


    }

    public String buildUpdateQuery(CustomerFull customer){
        String query = " SET ";
        if(customer.firstName != null) query += " FirstName = '" + customer.firstName + "',";
        if(customer.lastName != null) query += " LastName = '" + customer.lastName + "',";
        if(customer.company != null) query += " Company = '" + customer.company + "',";
        if(customer.address != null) query += " Address = '" + customer.address + "',";
        if(customer.city != null) query += " City = '" + customer.city + "',";
        if(customer.state != null) query += " State = '" + customer.state + "',";
        if(customer.country != null) query += " Country = '" + customer.country + "',";
        if(customer.postalCode != null) query += " PostalCode = '" + customer.postalCode + "',";
        if(customer.phone != null) query += " Phone = '" + customer.phone + "',";
        if(customer.fax != null) query += " Fax = '" + customer.fax + "',";
        if(customer.email != null) query += " Email = '" + customer.email + "',";
        if(customer.supportRepId != 1) query += " SupportRepId = '" + customer.supportRepId + "',";
        query = query.substring(0, query.length() - 1);
        return query;
    }
}
