package com.example.Task5.controllers;

import com.example.Task5.data_access.DataRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {
    DataRepository drep = new DataRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("artists", drep.getRandomArtists());
        model.addAttribute("songs", drep.getRandomSongs());
        model.addAttribute("genres", drep.getRandomGenres());
        return "index";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchSong(@RequestParam("query") String term, Model model){
        System.out.println("PARAM IS: " + term);
        model.addAttribute("song", drep.getSong(term));
        return "search";
    }
}
