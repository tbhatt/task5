package com.example.Task5.controllers;

import com.example.Task5.data_access.CustomerAccess;
import com.example.Task5.models.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
    CustomerAccess ca = new CustomerAccess();

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomers(){
        System.out.println("Getting all customers");
        return ca.getAllCustomers();
    }

    @RequestMapping(value = "/customers/country", method = RequestMethod.GET)
    public ArrayList<CountryStats> getCountryCount(){
        System.out.println("Getting count for countries");
        return ca.getCountryCount();
    }

    @RequestMapping(value = "/customers/billing", method = RequestMethod.GET)
    public ArrayList<BillingStats> getBillingCount(){
        System.out.println("Getting billings for customers");
        return ca.getTotalBilling();
    }

    @RequestMapping(value = "/customers/genre", method = RequestMethod.GET)
    public ArrayList<CustomerGenre> getGenreCount(){
        System.out.println("Getting genre count for customers");
        return ca.getCustomerGenre();
    }

    @RequestMapping(value = "/customers/update", method = RequestMethod.POST)
    @ResponseBody
    public Customer updateCustomer(@RequestBody CustomerFull customer){
        System.out.println("Updating customer with id: " + customer.customerId);
        return ca.updateCustomer(customer);
    }

    @RequestMapping(value = "/customers/add", method = RequestMethod.POST)
    @ResponseBody
    public String addCustomer(@RequestBody CustomerFull customer){
        System.out.println("Adding a new customer");
        ca.addCustomer(customer);
        return "Added a customer with ID: " + customer.customerId;
    }
}
